const jestHack = function () {
  // avoid jest open handle error
  return new Promise(resolve => setTimeout(() => resolve(), 500))
}

module.exports = jestHack