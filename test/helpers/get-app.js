const http = require('http')
const listen = require('test-listen')
const createApp = require('../../src/app')

module.exports = async (startServer = true) => {
  const app = await createApp()

  if (!startServer) {
    return { app }
  }

  const server = http.createServer(app)
  app.set('url', await listen(server))

  return {
    app,
    server,
  }
}
