const createApp = require('./app')

createApp()
  .then(app => {
    const port = process.env.PORT || process.env.API_PORT || 3000
    const server = app.listen(port)

    server.on('listening', () =>
      console.log('Subscription app started on http://localhost:%d', port)
    )

    return {
      app,
      server,
    }
  })
