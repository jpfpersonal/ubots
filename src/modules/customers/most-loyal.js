const { sortBy, find } = require('lodash')
const Products = require('../products/model')
const Customers = require('./model')

module.exports = () => async (req, res, next) => {
  try {
    const [customers, history] = await Promise.all([
      Customers.find(),
      Products.find(),
    ])

    const groupByCustomer = history.reduce((acc, order) => {
      if (!acc[order.cliente]) {
        acc[order.cliente] = {
          cpf: order.cliente,
          profile: {},
          n_orders: 0,
        }
      }

      acc[order.cliente].profile = find(customers,
        c => c.cpf === order.cliente)
      acc[order.cliente].n_orders += 1

      return acc
    }, {})

    const sortedHistory = sortBy(groupByCustomer, c => -1 * c.n_orders)

    return res.status(200).json(sortedHistory)
  } catch (err) {
    console.error(err)
    return next(err)
  }
}