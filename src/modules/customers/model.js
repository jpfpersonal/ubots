const { http, normalizeCpf } = require('../../core/base-model')

const find = () =>
  http
    .get('/598b16291100004705515ec5')
    .then(({ data }) => data.map(d => ({
      // eslint-disable-next-line
      ...d,
      cpf: normalizeCpf(d.cpf),
    })))

module.exports = {
  find,
}
