const { Joi, validate } = require('../../core/validation')
const mostLoyal = require('./most-loyal')
const getTopBuyer = require('./top-buyer')
const biggestBuyers = require('./biggest-buyers')

module.exports = function (app) {

  // sort by best total value in buys
  app.get('/customers/biggest-buyers',
    biggestBuyers(),
  )

  // get best buyer 2016
  app.get('/customers/top-single-buyer/:year',
    validate.params(Joi.object({
      year: Joi.number().required(),
    })),

    getTopBuyer(),
  )

  // sort by loyalty
  app.get('/customers/most-loyal',
    mostLoyal(),
  )
}
