const { sortBy, find } = require('lodash')
const Products = require('../products/model')
const Customers = require('./model')

module.exports = () => async (req, res, next) => {
  try {
    const [customers, history] = await Promise.all([
      Customers.find(),
      Products.find(),
    ])

    const customerCache = {}

    const groupByCustomer = history.reduce((acc, order) => {
      if (!acc[order.cliente]) {
        acc[order.cliente] = {
          cpf: order.cliente,
          profile: {},
          totalOrders: 0,
        }
      }

      if (!acc[order.cliente].profile && !customerCache[order.cliente]) {
        customerCache[order.cliente] = find(customers, c => c.cpf === order.cliente)
      }

      acc[order.cliente].profile = customerCache[order.cliente]
      acc[order.cliente].totalOrders += order.valorTotal

      return acc
    }, {})

    const sortedHistory = sortBy(groupByCustomer, c => -1 * c.totalOrders)

    return res.status(200).json(sortedHistory)
  } catch (err) {
    // console.error(err)
    return next(err)
  }
}