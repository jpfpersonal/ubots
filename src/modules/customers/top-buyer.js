const moment = require('moment')
const { find } = require('lodash')
const { NotFoundError } = require('../../errors')
const Products = require('../products/model')
const Customers = require('./model')

module.exports = () => async (req, res, next) => {
  try {
    const { year } = req.params

    const [customers, orders] = await Promise.all([
      Customers.find(),
      Products.find(),
    ])

    let topSingleBuyer = {
      cpf: '',
      profile: {},
      order: {},
      totalOrder: 0,
    }

    orders.reduce((acc, order) => {
      const orderYear = moment(order.data, 'DD-MM-YYYY')
      if (orderYear.format('YYYY') != year) {
        return acc
      }

      if (order.valorTotal > topSingleBuyer.totalOrder) {
        topSingleBuyer.cpf = order.cliente
        topSingleBuyer.order = order
        topSingleBuyer.totalOrder = order.valorTotal
      }

      return acc
    }, {})

    if (topSingleBuyer.totalOrder === 0) {
      throw new NotFoundError('No buying order available for this year')
    }

    topSingleBuyer.profile = find(customers, c => c.cpf === topSingleBuyer.cpf)

    return res.status(200).json(topSingleBuyer)
  } catch (err) {
    // console.error(err)
    next(err)
  }
}