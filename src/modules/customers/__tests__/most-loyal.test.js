const request = require('supertest')
const getApp = require('../../../../test/helpers/get-app')

describe('Biggest Buyers', function () {
  let app, server

  beforeAll(async function () {
    const { app: App, server: Server } = await getApp()
    app = App
    server = Server
  })

  afterAll(async function () {
    const mongo = app.get('mongooseClient')
    await mongo.close()
    await server.close()
  })

  it('Should list top 3 most loyal customers', async function () {
    await request(app)
      .get('/customers/most-loyal')
      .expect(200)
      .expect(({ body: loyal }) => {
        expect(loyal[0].n_orders).toBeGreaterThan(loyal[2].n_orders)
      })
  })
})