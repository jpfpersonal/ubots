const request = require('supertest')
const getApp = require('../../../../test/helpers/get-app')

describe('Biggest Buyers', function () {
  let app, server

  beforeAll(async function () {
    const { app: App, server: Server } = await getApp()
    app = App
    server = Server
  })

  afterAll(async function () {
    const mongo = app.get('mongooseClient')
    await mongo.close()
    await server.close()
  })

  it('Should return list', async function () {
    await request(app)
      .get('/customers/biggest-buyers')
      .expect(200)
  })

  it('Sorts customers from biggest to smallest buyer', async function () {
    await request(app)
      .get('/customers/biggest-buyers')
      .expect(({ body: list }) => {
        expect(list[0].totalOrders).toBeGreaterThan(list[list.length-1].totalOrders)
      })
  })
})