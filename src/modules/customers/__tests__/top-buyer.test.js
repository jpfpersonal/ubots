const request = require('supertest')
const getApp = require('../../../../test/helpers/get-app')

describe('Top buyer from', function () {
  let app, server

  beforeAll(async function () {
    const { app: App, server: Server } = await getApp()
    app = App
    server = Server
  })

  afterAll(async function () {
    const mongo = app.get('mongooseClient')
    await mongo.close()
    await server.close()
  })

  it('Should provide error message in year without data', async function () {
    await request(app)
      .get('/customers/top-single-buyer/2020')
      .expect(404)
  })

  it('Should find top buyer in 2016', async function () {
    await request(app)
      .get('/customers/top-single-buyer/2016')
      .expect(200)
      .expect(({ body }) => {
        expect(body).toHaveProperty('profile.nome', 'Pamela')
        expect(body).toHaveProperty('profile.id', 6)
      })
  })
})