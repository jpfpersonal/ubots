const { http, normalizeCpf } = require('../../core/base-model')

const find = () =>
  http
    .get('/598b16861100004905515ec7')
    .then(({ data }) => data.map(d => ({
      ...d,
      cliente: normalizeCpf(d.cliente),
    })))

module.exports = {
  find,
}
