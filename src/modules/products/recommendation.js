const { find } = require('lodash')
const { NotFoundError } = require('../../errors')
const Customers = require('../customers/model')
const Products = require('./model')

module.exports = () => async (req, res, next) => {
  try {
    const { customer_id } = req.params

    const customers = await Customers.find()
    const customer = find(customers, c => c.id == customer_id)

    if (!customer) {
      throw new NotFoundError('Customer not found')
    }

    const orders = await Products.find()

    const customerProducts = {}

    orders.forEach(o => {
      if (o.cliente !== customer.cpf) {
        return
      }

      o.itens.reduce((acc, product) => {
        if (!customerProducts[product.produto]) {
          customerProducts[product.produto] = {
            ...product,
            quantity: 0,
          }

          customerProducts[product.produto].quantity += 1
        }
      }, {})
    })

    const keys = Object.keys(customerProducts)
    const shuffleRecommendation = Math.floor(Math.random() * keys.length)
    const recommendation = customerProducts[keys[shuffleRecommendation]]

    return res.status(200).json(recommendation || {})
  } catch (err) {
    // console.error(err)
    next(err)
  }
}
