const { Joi, validate } = require('../../core/validation')
const recommendation = require('./recommendation')

module.exports = function (app) {

  // recommend a product to customer
  app.get('/products/:customer_id/recommendations',
    validate.params(Joi.object({
      customer_id: Joi.number().required(),
    })),

    recommendation(),
  )

}
