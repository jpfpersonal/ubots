const request = require('supertest')
const { find, omit } = require('lodash')
const getApp = require('../../../../test/helpers/get-app')
const Products = require('../model')

describe('Product recommendation', function () {
  let app, server

  beforeAll(async function () {
    const { app: App, server: Server } = await getApp()
    app = App
    server = Server
  })

  afterAll(async function () {
    const mongo = app.get('mongooseClient')
    await mongo.close()
    await server.close()
  })

  it('Should find customer', async function () {
    await request(app)
      .get('/products/11/recommendations')
      .expect(404)
  })

  it('Should recommend a product within customer\'s buying history', async function () {
    const { body: recommend } = await request(app)
      .get('/products/5/recommendations')
      .expect(200)

    expect(recommend).toBeTruthy()

    const history = await Products.find()

    const itemsFromHistory = []
    history.forEach(order => {
      if (order.cliente === '00000000005') {
        itemsFromHistory.push(...order.itens)
      }
    })
    const productFromHistory = find(itemsFromHistory, item => item.produto === recommend.produto)

    expect(productFromHistory).toBeTruthy()
    expect(JSON.stringify(productFromHistory))
      .toEqual(JSON.stringify(omit(recommend, ['quantity'])))
  })
})