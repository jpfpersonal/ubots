const messages = {
  notFound: 'Não encontrado ou não existe.',
  validation: 'Parâmetros enviados são inválidos.',
  duplicity: 'Um ou mais campos estão duplicados',
  unauthorized: 'Não autorizado',
  permission: 'Permissão negada',
  general: 'Erro desconhecido',
}

class GeneralError extends Error {
  constructor(message, status, implementationContext) {
    super(message || messages.general)
    this.name = 'GeneralError'
    Error.captureStackTrace(this, implementationContext || GeneralError)
    this.status = status || 500
  }

  toJSON() {
    return {
      message: this.message,
    }
  }
}

class NotFoundError extends GeneralError {
  constructor(message, errors) {
    super(message || messages.notFound, 404, NotFoundError)
    this.name = 'NotFoundError'
    this.errors = errors
  }
}

class BadRequestError extends GeneralError {
  constructor(message, errors) {
    super(message || messages.validation, 400, BadRequestError)
    this.name = 'BadRequestError'
    this.errors = errors
  }
}


class ValidationError extends GeneralError {
  constructor(message, errors) {
    super(message || messages.validation, 400, ValidationError)
    this.name = 'ValidationError'
    this.errors = errors
  }
}

class DuplicityValidationError extends GeneralError {
  constructor(message, errors) {
    super(message || messages.duplicity, 400, DuplicityValidationError)
    this.name = 'DuplicityValidationError'
    this.errors = errors
  }
}

class UnauthorizedError extends GeneralError {
  constructor(message, errors) {
    super(message || messages.unauthorized, 401, UnauthorizedError)
    this.name = 'UnauthorizedError'
    this.errors = errors
  }
}

class PermissionError extends GeneralError {
  constructor(message, errors) {
    super(message || messages.permission, 403, PermissionError)
    this.name = 'PermissionError'
    this.errors = errors
  }
}

module.exports = {
  messages,
  GeneralError,
  NotFoundError,
  BadRequestError,
  ValidationError,
  DuplicityValidationError,
  UnauthorizedError,
  PermissionError,
}
