const Joi = require('joi')
const validate = require('express-joi-validation').createValidator({ passError: true })

module.exports = {
  Joi,
  validate,
}
