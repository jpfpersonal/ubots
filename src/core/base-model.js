const axios = require('axios')

const http = axios.create({
  baseURL: 'http://www.mocky.io/v2',
})

const normalizeCpf = cpf => cpf.match(/\d+/g).join('').substr(-11)

module.exports = {
  http,
  normalizeCpf,
}