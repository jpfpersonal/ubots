const { readdirSync, statSync } = require('fs')
const path = require('path')

module.exports = async function (app) {
  const source = 'src/modules'
  readdirSync(source)
    .filter(dir => statSync(path.join(source, dir)).isDirectory())
    .forEach(async dir => {
      // register routes for modules ${dir}
      require(`./modules/${dir}`)(app, dir)
    })
}
